fetch("https://jsonplaceholder.typicode.com/todos")
  .then((response) => response.json())
  .then((json) => console.log(json));


 // Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API

 fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then((response) => response.json())
  .then((json) => console.log(json));



  // Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
  fetch("https://jsonplaceholder.typicode.com/todos", {
  
  method: "POST",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    title: "Created to do list Item",
    id: 201,
    userId: 1,
    completed: false
  })
})

.then(response=>response.json())
.then(json=>console.log(json))


 // Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

 fetch("https://jsonplaceholder.typicode.com/todos/1", {
  
  method: "PUT",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    title: "Updated to do list Item",
    description:"To update the my to do list with a differenct data structure",
    status: "Pending",
    dateCompleted:"Pending",
    userID:1
  })
})

.then(response=>response.json())
.then(json=>console.log(json));


//  Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
// Update a to do list item by changing the status to complete and add a date when the status was changed.


fetch("https://jsonplaceholder.typicode.com/todos/1", {
  
  method: "PATCH",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    status: "Complete",
    dateCompleted:"07/09/21"
    
  })
})

.then(response=>response.json())
.then(json=>console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/1",{
  method:"DELETE"
})