// SECTION - JS Symchronous cs Asynchronous
// Javascript is by default is synchronous, meaning one statment will be executed at a time
// this can be proven when a statement has an error. Javascript will not proceed to the next statement
/*console.log("hello Wolrd");
consle.log(:"Hello Again");
console.log("Goodbye");
*/


// when statements take time to process, this slows down our code.
// An example of this is when loops are used on a large amount of information or when fetching data form databases
// We might not notice to it due to the fast processing power of devices 
// This is the reason some websites don't instantly load and we only see a white screen at times while the application is still waiting for all the code to be executed

/*for(let i=1; i<=1500; i++){
	console.log(i)
};

// this will only be displayed aafter the loops is finished
console.log("it's me again");*/


// Fetch API - allows us to asycnhronously request for a resource(data);
// asycnhronous - allows executinh of codes simultaneousl prioritizing on less resource-consumn=in codes while more complex and more resource-consuming run at the back
// A "Promise" is an object that represents the eventual completion or failure of an asynchronous function and its resuting value 
// console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

fetch("https://jsonplaceholder.typicode.com/posts")
// .then(response => console.log(response.status));


// we use the "json" method from the response object to convert the data retreived into JSON format to be used in our application
.then(response=>response.json())
// using multiple then methods will result into "promise chain"
// then we can now access it and log in the console our desired output
.then(json=>console.log(json));



console.log("Hello");
console.log("Hello Again");
console.log("Goodbye");



// ASYNC-AWAIT
// "asycn" and "await" keywords are other approach that can be used to perform asynchronous javascript 
// used in functions to indicate which portions of the codes  should be awaited for
async function fetchData(){
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json()
	console.log(json);
};


fetchData();


console.log("hello")

// Creating a Post
/*
	SYNTZX
	fetch(URL, options )
*/
// create a new post following REST IP (create, /post, Post)

fetch("https://jsonplaceholder.typicode.com/posts", {
	// setting request method into "POST"
	method: "POST",
	// settiing request headers to be sent to the bakcend
	// specified that the content headers will be sending JSON 	structure for its content
	headers: {
		"Content-Type": "application/json"
	},
	// set content/body data of the "request" object to be sent to the backend
	// JSON.stringig converst the object into stringified JSON
	body: JSON.stringify({
		comp: "New Post",
		body: "Hello World",
		userId: 1
	})
})

.then(response=>response.json())
.then(json=>console.log(json))



fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers:{
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated Post",
		body: "Hello again",
		userId: 1
	})
})
.then(response=>response.json())
.then(json=>console.log(json))

// SECTION - update post(PATCH)
/*
	The difference between PUT and PATCH is the number of properties being updated
	PATCH is used to update a single propery while maintaining the other properties
	PUT is used when all of the properties need to be updated, or the whole document itself
*/
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers:{"Content-Type": "application/json"},
	body: JSON.stringify({
		title: "Corrected Post",
	})
})
.then(response=>response.json())
.then(json=>console.log(json))

// SECTION - deleting of a resource
fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method:"DELETE"
})

// SECTION - Filter posts
// the data can be filtered by sending the useId along with the URL
// information sent via the url can be done by adding the question mark symbol(?)
fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
.then(response=>response.json())
.then(json=>console.log(json));


// multiple search
fetch("https://jsonplaceholder.typicode.com/posts?userId=1&&id=2")
.then(response=>response.json())
.then(json=>console.log(json));

// Retreiving comments for a specific post/acccessing nested/embedded comments

fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then(response=>response.json())
.then(json=>console.log(json));




